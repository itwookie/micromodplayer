#ifndef __TRACKER_H__
#define __TRACKER_H__
#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// delay as 1/playback frequency in nanoseconds
// at a playback rate of 44100 Hz this value will be 22675 ns
// at a playback rate of 16000 Hz this value will be 62500 ns
// at a playback rate of 4800 Hz this value will be 208333 ns
#define PLAYBACK_PERIOD (22'675)
#define INTERRUPTS_PER_TICK (20'000'000/PLAYBACK_PERIOD)

// 564 is twice a calulated value involving the sample rate and c-3 frequency (16.5kHz)
// ofc i did not write down anything - what would life be without magic numbers ;D
// 44100 Hz (22'675 ns, playback period), 16500 Hz (60'606 ns), c-3 = 214
// playback frequency / c-3 (hz) * c-3 (period) = 571
// == c-3 (ns) / playback period * c-3 (period)
// is this universal?
#define PERIOD_MULTIPLIER (60'606 * 214 / PLAYBACK_PERIOD)

// Additional scale (L-Shifting the mix Value after averaging)
// In case the track appears way too silent
#define MIX_OUTPUT_SCALE    ( 0 )
// Additional divider (R-Shifting the mix Value after scaling)
// In case the track is overloaded
#define MIX_OUTPUT_DIVIDER  ( 3 )
// Value bit-mask (Preventing damage by cutting off values after dividing)
// Since some DMAs might only take 10-12 bit values
#define MIX_OUTPUT_MASK     ( 0b0000001111111111 )
// The actual Mixing formula
// This is a define so you can fine-tune it is necessary
// The most basic value for this would be ( X )
// The or causes the shifts to be signed (not relevant for ucs but if you want to import the result as PCM into e.g. Audacity)
#define MIX_OUTPUT( X )     ( ( ( ( X ) << MIX_OUTPUT_SCALE) >> MIX_OUTPUT_DIVIDER ) & MIX_OUTPUT_MASK \
                              | ( ( ( X ) & 0x8000 ) > 0 ? ( 0xffff & ~MIX_OUTPUT_MASK) : 0x0 ) )
//#define MIX_OUTPUT( X )     ( ( X ) & MIX_OUTPUT_MASK )

#define printf_s printf

extern uint16_t periodtable[5][12];

typedef union {
    uint32_t raw;
    struct { //inverted compared to spec, since Intel order is inverse to Amiga (speced for)
        unsigned int effect_data    : 8;
        unsigned int effect         : 4;
        unsigned int instrument_low : 4;
        unsigned int period        : 12;
        unsigned int instrument     : 4;
    } data;
} NOTE;
typedef struct {
    uint8_t name[22];
    uint16_t wordLength;
    uint8_t fineTune;
    uint8_t volume;
    uint16_t wordLoopStart;
    uint16_t wordLoopEnd;
} INSTRUMENT;
typedef struct {
    uint8_t title[20];
    INSTRUMENT sample[31];
    uint8_t length; //length in patterns
    uint8_t restart;
    uint8_t patternSequenze[128];
    uint8_t id[4];
    NOTE note[];
} MOD;

typedef struct {
    int16_t* addr;
    int16_t* loopS;
    int16_t* loopE;
} SAMPLE;
typedef struct {
    SAMPLE* sample;
    int16_t* playing;
    uint32_t timeToNextValue = periodtable[3][0] * PERIOD_MULTIPLIER; // 564 * period value ns ; default = c-3
    uint32_t timeCounter=0;
    uint8_t volume=64;
} CHANNEL;
typedef struct {
    uint8_t pp; //pattern currently playint
    uint8_t sp=0; //sub pointer, indexing the row
    uint8_t ppp=0; //pattern playlist pointer, indexing the pattern order list
    uint8_t done=false;

    NOTE* currentNote = 0;
    uint8_t channels=4; //has to be calculated based on file size since ID is not very clear on this
    int16_t* sampleoffset;
    uint32_t patternsize; // in notes (4 bytes)
    SAMPLE samples[31];

    //one tick is supposed to be .02 s
    uint8_t ticks=0;
    uint8_t ticksPerRow=6;
    uint16_t interruptCounter=0;

    CHANNEL channel[8];
    int16_t mixValue;
} PLAYER;

inline void player_interrupt();
MOD* player_load(uint8_t* modFile, size_t modSize, PLAYER** playerPtr);
///generate the amount of words and places them in some memory specified by basePtr (usually a DMA)
inline void player_loadDMA(int16_t* basePtr, uint32_t words);
///this variation is a tiny bit more expensive, but will listen to the song end and pad the rest
/// of the DMA with zeros
/// returns 1 if the song ended during this fill,
///         -1 if the song ended before this fill (all zero output)
///         0 otherwise
int8_t player_loadDMAnorepeat(int16_t* basePtr, size_t words);

#endif // __TRACKER_H__
