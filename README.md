# Micro Mod Player

This project is currently in development.

The goal will ultimately be to have a micro mod player that runs on most microchips with minimal adaptations.

It will require either a pre-processes mod that has intel byte order, or a pointer to writeable memory, in order to convert from amiga byte order to intel byte order.

Playing is planned to be as simple as `initPlayer(uint8_t* modBytes)`. Currently playback is trimmed to 44100 kHz, requiring a interrupt every 22.675 us.
It will mix all channels into one mono stream to be played on a single speaker or beeper, by a DAC.

Project is WIP and will be tested on a PIC24.