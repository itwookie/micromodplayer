#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "tracker.hpp"

//c++ libs but universal sleep
#include <chrono>
#include <thread>
#define sleepn(X) std::this_thread::sleep_for(std::chrono::nanoseconds( X ))
#define sleep(X) std::this_thread::sleep_for(std::chrono::milliseconds( X ))

/** temporary method as modFile will be in Flash */
uint8_t flash[8192];
//num words to write at once, full DMA on the test pic has 8kiB = 4ki Words
#define DMA_SIZE 2048
int16_t dma[DMA_SIZE];
long readMod() {
    FILE* file = fopen("mario_chip_tune.mod", "rb");
    long fsize;
    fseek(file, 0, SEEK_END);
    fsize = ftell(file);
    fseek(file, 0, SEEK_SET);  /* same as rewind(f); */
    fread(flash, 1, fsize, file);
    fclose(file);
    return fsize;
}
// Will be a raw file: 8-bit unsigned, 44100 Hz playback
FILE* speaker = fopen("audio.raw", "wb");
PLAYER* player;
MOD* track;

int main() {

    long filesize = readMod();
    if (filesize < 0) {
            printf_s( "Could not read file!" );
        return -1;
    }
    uint8_t* flashptr = (uint8_t*)flash;
    track = player_load(flashptr, filesize, &player);

    printf_s("Playing at %i ticks per row with a interrupt period of %i ns at %i interrupts per tick\n", player->ticksPerRow, PLAYBACK_PERIOD, INTERRUPTS_PER_TICK);

    printf_s("Title: %.20s\n", track->title);
//    int i;
//    for (i=0; i<31; i++) {
//        printf_s("Sample: %.22s\n", track->sample[i].name);
//        printf_s("  L: %d, <%d -loop- %d>\n", track->sample[i].wordLength, track->sample[i].wordLoopStart, track->sample[i].wordLoopEnd);
//    }
    printf_s("Looping: %02X\n", track->restart);
    printf_s("ID: %.4s\n", track->id);

    uint64_t deltaTime; //count maximum interrupt time (on my Intel PC with 4.3 GHz)
    uint64_t totalTimeNs=0;
    uint32_t totalTimeMs=0;
    std::chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now(), t1;
    while (player_loadDMAnorepeat(dma, DMA_SIZE)==0) {
        t1 = std::chrono::steady_clock::now();
        deltaTime = std::chrono::duration_cast<std::chrono::nanoseconds>( t1 - t0 ).count();
        printf_s( "Filling the buffer took %i ns (@16Mhz ~ %.0f us)\n", deltaTime, deltaTime*0.22);
        totalTimeNs+=deltaTime;
        while (totalTimeNs > 1000000) {
            totalTimeNs -= 1000000;
            totalTimeMs ++;
        }

        //fwrite(&(player->mixValue), 2, 1, speaker);
        fwrite(dma, sizeof(dma[0]), DMA_SIZE, speaker);

        t0 = std::chrono::steady_clock::now();
    }
    fflush(speaker);
    fclose(speaker);

    printf_s( "Writing took %i.%06i ms\n", totalTimeMs, totalTimeNs );
    totalTimeMs *= 219;
    totalTimeNs *= 219;
    while (totalTimeNs > 1000000) {
        totalTimeNs -= 1000000;
        totalTimeMs ++;
    }
    printf_s( "Expected total at 16MHz uc (assuming host is 3.5GHz): %i.%06i ms\n", totalTimeMs, totalTimeNs );

return 0; }
