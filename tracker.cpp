#include "tracker.hpp"

// https://www.fileformat.info/format/mod/corion.htm
// https://eblong.com/zarf/blorb/mod-spec.txt
// http://lclevy.free.fr/mo3/mod.txt

uint16_t periodtable[5][12] = {
//         c     c#    d     d#    e     f     f#    g     g#    a     a#    b
/* 0 */ { 1712, 1616, 1524, 1440, 1356, 1280, 1208, 1140, 1076, 1016,  960,  906 },
/* 1 */ {  856,  808,  762,  720,  678,  640,  604,  570,  538,  508,  480,  453 },
/* 2 */ {  428,  404,  381,  360,  339,  320,  302,  285,  269,  254,  240,  226 },
/* 3 */ {  214,  202,  190,  180,  170,  160,  151,  143,  135,  127,  120,  113 },
/* 4 */ {  107,  101,   95,   90,   85,   80,   75,   71,   67,   63,   60,   56 }
};
//uint16_t volumetable[] = { 0, 1750, 2503, 2701, 2741, 2781, 2944, 2964, 2981, 3000, 3017, 3034, 3052, 3070, 3207, 3215, 3224,
//                              3232, 3240, 3248, 3256, 3263, 3271, 3279, 3287, 3294, 3303, 3310, 3317, 3325, 3458, 3462, 3466,
//                              3469, 3473, 3478, 3481, 3484, 3489, 3492, 3495, 3499, 3502, 3506, 3509, 3513, 3517, 3520, 3524,
//                              3528, 3532, 3534, 3538, 3543, 3545, 3549, 3552, 3556, 3558, 3563, 3565, 3570, 3573, 3577, 3580 };
MOD* _mmp_track;
PLAYER _mmp_player;
void initPlayer();
inline void playRow();
inline void playTick();
inline void playNotes();
/// Available from the header (just here for simplicity)
// MOD* player_load(uint8_t* modFile, size_t modSize, PLAYER** playerInstance);
// inline void player_interrupt();
// inline void player_loadDMA(int16_t* basePtr, uint32_t words);
// inline int8_t player_loadDMAnorepeat(int16_t* basePtr, uint32_t words);

#define CURRENT_NOTE _mmp_track->note[(_mmp_player.pp*_mmp_player.patternsize) + (_mmp_player.sp*_mmp_player.channels)]

/** clock interrupt, update analog output value */
inline void player_interrupt() {
    if (_mmp_player.currentNote == 0) playRow();
    else if ((++_mmp_player.interruptCounter) >= INTERRUPTS_PER_TICK) {
        _mmp_player.interruptCounter = 0;

        if ((++_mmp_player.ticks) >= _mmp_player.ticksPerRow) {
            _mmp_player.ticks = 0;

            playRow();
        }

        playTick();
    }
    playNotes(); //updates wave
}

inline void player_loadDMA(int16_t* basePtr, size_t words) {
    //one player_interrupt generates one sample at 16 bits (1 word) intel byte order
    int16_t* endPtr = (basePtr+words);
    for (;basePtr < endPtr; basePtr++) {
        player_interrupt();
        *basePtr = _mmp_player.mixValue;
    }
}
int8_t player_loadDMAnorepeat(int16_t* basePtr, size_t words) {
    //one player_interrupt generates one sample at 16 bits (1 word) intel byte order
    int16_t* endPtr = (basePtr+words);
    if (_mmp_player.done) {
        for (;basePtr < endPtr; basePtr++)
            *basePtr = 0;
        return -1;
    }
    for (;basePtr < endPtr; basePtr++) {
        if (_mmp_player.done) {
            *basePtr = 0;
        } else {
            player_interrupt();
            *basePtr = _mmp_player.mixValue;
        }
    }
    return _mmp_player.done ? 1 : 0;
}

MOD* player_load(uint8_t* modFile, size_t modSize, PLAYER** playerPtr) {
//    _mmp_track->raw = *(modFile);
    _mmp_track = (MOD*)modFile;
    *playerPtr = &_mmp_player;

    //FLT8 is fucked:
    // channel 1-4 are stored in pattern 2n+0
    // channel 5-8 are stored in pattern 2n+1
    //where n is the logical pattern number
    uint8_t i,j;
    uint32_t nn;
    void* lastAddr = modFile+modSize;
    ///detect channels by ID
    //detect channels
    _mmp_player.channels = 4; // standard - M.K. or FLT4 or M!K! or 4CHN
    if (_mmp_track->id[0] == 'F' || _mmp_track->id[0] == 'T') {//FLT4, FLT8 TDZx [123]
        _mmp_player.channels = _mmp_track->id[3]-'0';
    } else if (_mmp_track->id[0] == 'C') {//CD81
        _mmp_player.channels = 8;
    } else if (_mmp_track->id[1] == 'C') {//xCHN [2456789]
        _mmp_player.channels = _mmp_track->id[0]-'0';
    } else if (_mmp_track->id[2] == 'C') {//xxCH
        _mmp_player.channels = (_mmp_track->id[0]-'0')*10+(_mmp_track->id[1]-'0');
    } //see http://lclevy.free.fr/mo3/mod.txt
    _mmp_player.patternsize = _mmp_player.channels << 6;// *rows in pattern, not counting note size, as the patten length in notes is more interesting

    j=0; //highest pattern index
    for (i=0; i < _mmp_track->length; i++)
        if (_mmp_track->patternSequenze[i] > j)
            j = _mmp_track->patternSequenze[i];

    ///find last note index
    nn = (j+1) * (_mmp_player.patternsize); //max is the number of notes
    //convert Amiga HHLLHHLL to Intel LLHHLLHH
    NOTE* n = _mmp_track->note;
    INSTRUMENT* instrument = _mmp_track->sample;
    for (j=0; j<31; j++, instrument++) { //convert length values in sample data
        instrument->wordLength    = ((instrument->wordLength    & 0xFF00)>>8)|((instrument->wordLength    & 0x00FF)<<8);
        instrument->wordLoopStart = ((instrument->wordLoopStart & 0xFF00)>>8)|((instrument->wordLoopStart & 0x00FF)<<8);
        instrument->wordLoopEnd   = ((instrument->wordLoopEnd   & 0xFF00)>>8)|((instrument->wordLoopEnd   & 0x00FF)<<8);
    }
    for (; n < lastAddr; n++) {
         n->raw = ((n->raw & 0xFF00FF00)>>8)|((n->raw & 0x00FF00FF)<<8);
         n->raw = ((n->raw & 0xFFFF0000)>>16)|((n->raw & 0x0000FFFF)<<16);
    }
    //<-- at this point the note pointer is post the last note, hence pointing at the first sample
    _mmp_player.sampleoffset = ((int16_t*)(_mmp_track->note + nn));

    //init sample pointer
    _mmp_player.samples[0].addr = _mmp_player.sampleoffset;
    _mmp_player.samples[0].loopS = _mmp_player.samples[0].addr+ (_mmp_track->sample[0].wordLoopStart);
    _mmp_player.samples[0].loopE = _mmp_player.samples[0].addr+ (_mmp_track->sample[0].wordLoopEnd);
    for (int i = 1; i < 31; i++) {
        _mmp_player.samples[i].addr =
            _mmp_player.samples[i-1].addr + (_mmp_track->sample[i-1].wordLength);
        _mmp_player.samples[i].loopS = _mmp_player.samples[i].addr+ (_mmp_track->sample[i].wordLoopStart);
        _mmp_player.samples[i].loopE = _mmp_player.samples[i].addr+ (_mmp_track->sample[i].wordLoopEnd);
    }

    return _mmp_track;
}

inline void playTick() {

    int8_t stmp;  //signed temp value
    uint8_t c; //channel counter
    CHANNEL* chn;

    _mmp_player.currentNote = &CURRENT_NOTE;
    chn = _mmp_player.channel;

    ///handle instruments, notes and effx
    for (c = 0; c < _mmp_player.channels && c < 8; c++, _mmp_player.currentNote++, chn++) {
        // Process effects that shall happen at the start of the row
        switch (_mmp_player.currentNote->data.effect) {
        case 0xA: { // Volume slide
            //actually having both values set is illegal, but i trust trackers don't allow that - we'll just increase in that case
            if ((_mmp_player.currentNote->data.effect_data & 0xf0)>0) {
                stmp = chn->volume+((_mmp_player.currentNote->data.effect_data & 0xf0)>>4);
                chn->volume = stmp>64?64:stmp;
            }
            else if ((_mmp_player.currentNote->data.effect_data & 0x0f)>0) {
                stmp = chn->volume-(_mmp_player.currentNote->data.effect_data & 0x0f);
                chn->volume = stmp<0?0:stmp;
            }
        }
        break;
        default:
        break;
        }
    }
}

inline void playRow() {

    int8_t stmp;  //signed temp value
    uint8_t utmp; //unsigned temp value
    uint8_t c; //channel counter
    uint8_t instrument;
    CHANNEL* chn;

    if (_mmp_player.currentNote == 0 ) {
        _mmp_player.pp = _mmp_track->patternSequenze[_mmp_player.ppp];
    } else { //advance row

        // Process effects that shall happen at the END of the row
        _mmp_player.currentNote = &CURRENT_NOTE;
        chn = _mmp_player.channel;
        ///handle instruments, notes and effx
        for (c = 0; c < _mmp_player.channels && c < 8; c++, _mmp_player.currentNote++, chn++) {
            switch (_mmp_player.currentNote->data.effect) {
            case 0xB: { // Position Jump
                utmp = (_mmp_player.currentNote->data.effect_data);
                if (utmp<127) {
                    //jump to next pattern
                    if (utmp >= _mmp_track->length) { //jump beyond last pattern
                        if (_mmp_track->restart >= 127) {//track should not repeat, but mc does not support applications ending
                            _mmp_player.ppp = 0; //so repeat the whole song
                        } else
                            _mmp_player.ppp = _mmp_track->restart; //otherwise this byte tells us at what pattern to restart
                        _mmp_player.done = true;
                    } else {
                        _mmp_player.ppp = utmp;
                    }
                    _mmp_player.sp = -1; //go before first row in pattern -1 (follow following code will increase by 1)
                    _mmp_player.pp = _mmp_track->patternSequenze[_mmp_player.ppp];
                }
            }
            break;
            case 0xD: { // Pattern Break
                stmp = (_mmp_player.currentNote->data.effect_data & 0xf0)>>4; //has to be "positive" after shift
                utmp = (_mmp_player.currentNote->data.effect_data & 0x0f);
                if (stmp<6) {
                    utmp = stmp*10+utmp;
                    if (utmp < 64) {
                        //jump to next pattern
                        if ((++_mmp_player.ppp) >= _mmp_track->length) { //end reached
                            if (_mmp_track->restart >= 127) {//track should not repeat, but mc does not support applications ending
                                _mmp_player.ppp = 0; //so repeat the whole song
                            } else
                                _mmp_player.ppp = _mmp_track->restart; //otherwise this byte tells us at what pattern to restart
                            _mmp_player.done = true;
                        }
                        _mmp_player.sp = utmp-1; //selected row in pattern -1 (follow following code will increase by 1)
                        _mmp_player.pp = _mmp_track->patternSequenze[_mmp_player.ppp];
                    }
                }
            }
            break;
            default:
            break;
            }
        }

        if ((++_mmp_player.sp) >= 64) {
            //jump to next pattern
            if ((++_mmp_player.ppp) >= _mmp_track->length) { //end reached
                if (_mmp_track->restart >= 127) {//track should not repeat, but mc does not support applications ending
                    _mmp_player.ppp = 0; //so repeat the whole song
                } else
                    _mmp_player.ppp = _mmp_track->restart; //otherwise this byte tells us at what pattern to restart
                _mmp_player.done = true;
            }
            _mmp_player.sp = 0; //first row in pattern
            _mmp_player.pp = _mmp_track->patternSequenze[_mmp_player.ppp];
        }
        //otherwise currentNote* will be correctly incremented by playing notes
    }
    _mmp_player.currentNote = &CURRENT_NOTE;
    chn = _mmp_player.channel;
    ///handle instruments, notes and effx
    for (c = 0; c < _mmp_player.channels && c < 8; c++, _mmp_player.currentNote++, chn++) {
        if (_mmp_player.currentNote->data.period>0) {
            //_mmp_player.channel[c].timeToNextValue = 7159090 / (_mmp_player.currentNote->data.period << 1); // << to be honest: I have no idea what to do with the sample rate formula
            chn->timeToNextValue = PERIOD_MULTIPLIER * _mmp_player.currentNote->data.period;
            chn->timeCounter = 0;
            chn->volume = 64;
        }
        instrument = ((_mmp_player.currentNote->data.instrument)<<4) | ((_mmp_player.currentNote->data.instrument_low));
        if (instrument > 0) {
            chn->sample = &_mmp_player.samples[instrument -1];
            chn->playing = chn->sample->addr;
        }

        // Process effects that shall happen at the start of the row
        switch (_mmp_player.currentNote->data.effect) {
        case 0xC: { // Set volume
            utmp = (_mmp_player.currentNote->data.effect_data);
            chn->volume = utmp>64?64:0;
        }
        break;
        case 0xF: { // Set speed
            if (_mmp_player.currentNote->data.effect_data == 0) { //stop playback (if stop is not supported, repeat)
                _mmp_player.done = true;
                _mmp_player.pp=0;
                _mmp_player.ppp=0;
                _mmp_player.sp=0;
            }
            else if (_mmp_player.currentNote->data.effect_data<32) { //set speed in ticks per rpw
                _mmp_player.ticksPerRow = _mmp_player.currentNote->data.effect_data;
            }
            else { //set speed as BPM
               //TODO
            }
        }
        break;
        default:
        break;
        }
    }

}

/** generate output for notes in currently selected row (player.currentNote) */
inline void playNotes() {
    _mmp_player.currentNote = &CURRENT_NOTE;
    CHANNEL* channel = _mmp_player.channel;
    int32_t mixv = 0;
    for (int i=0; i < _mmp_player.channels && i < 8; i++, _mmp_player.currentNote++, channel++) {
        if (channel->sample == 0) {
            continue;
        }
        channel->timeCounter += PLAYBACK_PERIOD;
        while (channel->timeToNextValue>0 && channel->timeCounter >= channel->timeToNextValue) {
            channel->timeCounter -= channel->timeToNextValue;
            if ((++channel->playing)>=(channel->sample->loopE))
                channel->playing = channel->sample->loopS;
        }
//        mixv += ((int32_t(volumetable[channel->volume]) * (*(channel->playing))) / volumetable[64]);
        mixv += ((int32_t(channel->volume) * (*(channel->playing))) / 64);
    }
    _mmp_player.mixValue = mixv/_mmp_player.channels;
    _mmp_player.mixValue = MIX_OUTPUT( _mmp_player.mixValue );
}
